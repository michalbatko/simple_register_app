<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Registration form</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
    <script src="js/jquery-2.1.4.js"></script>
</head>

<body>

<div class="container">

    <form:form method="post" commandName="account" cssClass="form-signin" id="register-form">
        <h2 class="form-signin-heading">Please sign up</h2>

        <div class="form-errors">
            <form:errors path="username" cssClass="alert alert-danger" element="div"/>
            <form:errors path="password" cssClass="alert alert-danger" element="div"/>
        </div>

        <div class="form-username-group">
            <label for="inputUsername" class="sr-only">Username</label>
            <form:input path="username" id="inputUsername" cssClass="form-control" placeholder="Username" autofocus="autofocus"/>
        </div>

        <div class="form-password-group">
            <label for="inputPassword" class="sr-only">Password</label>
            <form:password path="password" id="inputPassword" cssClass="form-control" placeholder="Password"/>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up!</button>


    </form:form>

</div>
<script type="text/javascript">

    function collectFormData(fields) {
        var data = {};
        for (var i = 0; i < fields.length; i++) {
            var $item = $(fields[i]);
            data[$item.attr('name')] = $item.val();
        }
        return data;
    }

    function createErrorMsg(message) {
        $('<div/>', {
            class: 'alert alert-danger',
            text: message
        }).appendTo($('.form-errors'));
    }

    $(document).ready(function () {
        var $form = $('#register-form');

        // Ajax validation
        $form.bind('submit', function (e) {
            $.post('validate', collectFormData($form.find('input')), function (response) {
                $('.form-errors').empty();

                if (response.result == 'FAIL') {
                    for (var i = 0; i < response.items.length; i++) {
                        createErrorMsg(response.items[i].message);
                    }
                } else {
                    $form.unbind('submit');
                    $form.submit();
                }
            }, 'json');

            e.preventDefault();
            return false;
        });
    })

</script>
</body>
</html>
