<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Error</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <div  class="form-signin">
        <h2 class="form-signin-heading">Upsss... :(</h2>

        <div class="msg-div">A small problem was encountered - sorry about that!</div>

        <a href="register" class="btn btn-lg btn-primary btn-block">Try again</a>
    </div>

</div>

</body>
</html>
