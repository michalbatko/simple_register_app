package pl.mbatko.simpleregister.other;

public class Constants {
    public static final String MSG_PASSWORD_INVALID = "password.invalid";
    public static final String MSG_USERNAME_INVALID = "username.invalid";
    public static final String MSG_USERNAME_NOT_UNIQUE = "username.notunique";

    public static final String FIELD_USERNAME = "username";
    public static final String FIELD_PASSWORD = "password";
}
