package pl.mbatko.simpleregister.model;

import java.util.ArrayList;
import java.util.List;

public class ValidationResponse {

    private static final String SUCCESS = "SUCCESS";
    private static final String FAIL = "FAIL";

    private final String result;
    private final List<ValidationItem> items = new ArrayList<>();

    private ValidationResponse(String result) {
        this.result = result;
    }

    public static ValidationResponse success() {
        return new ValidationResponse(SUCCESS);
    }

    public static ValidationResponse fail() {
        return new ValidationResponse(FAIL);
    }

    public String getResult() {
        return result;
    }

    public void addItem(String field, String code, String message) {
        this.items.add(new ValidationItem(field, code, message));
    }

    public List<ValidationItem> getItems() {
        return items;
    }
}
