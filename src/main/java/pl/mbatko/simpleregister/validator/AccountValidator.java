package pl.mbatko.simpleregister.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.mbatko.simpleregister.other.Constants;
import pl.mbatko.simpleregister.model.WebAccount;

import java.util.Optional;

@Component
public class AccountValidator implements Validator {

    @Autowired
    private UsernameValidator usernameValidator;

    @Autowired
    private PasswordValidator passwordValidator;

    @Override
    public boolean supports(Class<?> clazz) {
        return WebAccount.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        WebAccount account = (WebAccount) target;

        Optional<String> usernameErrorMsg = usernameValidator.validate(account.getUsername());
        Optional<String> passwordErrorMsg = passwordValidator.validate(account.getPassword());

        if (usernameErrorMsg.isPresent()) {
            errors.rejectValue(Constants.FIELD_USERNAME, usernameErrorMsg.get());
        }
        if (passwordErrorMsg.isPresent()) {
            errors.rejectValue(Constants.FIELD_PASSWORD, passwordErrorMsg.get());
        }
    }
}
