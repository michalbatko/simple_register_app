package pl.mbatko.simpleregister.validator;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static pl.mbatko.simpleregister.other.Constants.MSG_PASSWORD_INVALID;

@Component
public class PasswordValidator {

    public Optional<String> validate(String password) {

        if (StringUtils.isEmpty(password) || !password.matches("[a-zA-Z0-9!@#$%^&*()]{8,}") || !password.matches(".*[0-9].*")
                || !password.matches(".*[A-Z].*") || !password.matches(".*[a-z].*") ) {
            return Optional.of(MSG_PASSWORD_INVALID);
        }

        return Optional.empty();
    }
}
