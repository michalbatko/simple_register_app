package pl.mbatko.simpleregister.validator;

import pl.mbatko.simpleregister.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;

import static pl.mbatko.simpleregister.other.Constants.MSG_USERNAME_INVALID;
import static pl.mbatko.simpleregister.other.Constants.MSG_USERNAME_NOT_UNIQUE;

@Component
public class UsernameValidator {

    @Autowired
    private AccountService accountService;

    public Optional<String> validate(String username) {

        if (StringUtils.isEmpty(username) || username.length() < 6 || !username.matches("\\w+")) {
            return Optional.of(MSG_USERNAME_INVALID);
        } else if (!accountService.isNameUnique(username)) {
            return Optional.of(MSG_USERNAME_NOT_UNIQUE);
        }

        return Optional.empty();
    }
}
