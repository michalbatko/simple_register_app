package pl.mbatko.simpleregister.service;

import pl.mbatko.simpleregister.model.WebAccount;

public interface AccountService {

    void save(WebAccount account);

    boolean isNameUnique(String username);
}
