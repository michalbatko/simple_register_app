package pl.mbatko.simpleregister.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.mbatko.simpleregister.dao.AccountDao;
import pl.mbatko.simpleregister.model.Account;
import pl.mbatko.simpleregister.model.WebAccount;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    public void save(WebAccount account) {

        accountDao.save(new Account(account.getUsername(), account.getPassword()));
    }

    public boolean isNameUnique(String username) {
        return accountDao.isNameUnique(username);
    }
}
