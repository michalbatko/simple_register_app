package pl.mbatko.simpleregister.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.mbatko.simpleregister.model.ValidationResponse;
import pl.mbatko.simpleregister.model.WebAccount;
import pl.mbatko.simpleregister.service.AccountService;
import pl.mbatko.simpleregister.validator.AccountValidator;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import static pl.mbatko.simpleregister.other.Constants.FIELD_USERNAME;
import static pl.mbatko.simpleregister.other.Constants.MSG_USERNAME_NOT_UNIQUE;

@Controller
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private static final String REGISTER = "register";
    private static final String SUCCESS = "success";

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountValidator accountValidator;

    @Autowired
    private MessageSource messageSource;


    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String init(Model model) {
        model.addAttribute("account", new WebAccount());

        return REGISTER;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String submit(@Valid WebAccount account, BindingResult result) {

        if (result.hasErrors() || !saveAccount(account, result)) {
            return REGISTER;
        }

        return SUCCESS;
    }

    @RequestMapping(value = "/validate", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ValidationResponse validate(@Valid WebAccount account, BindingResult result) {

        if (result.hasErrors()) {
            ValidationResponse response = ValidationResponse.fail();
            for (FieldError fieldError : result.getFieldErrors()) {
                response.addItem(fieldError.getField(), fieldError.getCode(), messageSource.getMessage(fieldError, null));
            }

            return response;
        }

        return ValidationResponse.success();
    }

    private boolean saveAccount(WebAccount account, BindingResult result) {
        try {
            accountService.save(account);
        } catch (ConstraintViolationException e) {
            logger.error("", e);
            result.rejectValue(FIELD_USERNAME, MSG_USERNAME_NOT_UNIQUE);

            return false;
        }
        return true;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addValidators(accountValidator);
    }
}
