package pl.mbatko.simpleregister.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    private static final String ERROR = "error";

    @ExceptionHandler(Exception.class)
    public String handleAllExceptions(Exception e) {
        logger.error("", e);
        return ERROR;
    }
}
