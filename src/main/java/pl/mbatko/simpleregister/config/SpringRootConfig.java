package pl.mbatko.simpleregister.config;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan({"pl.mbatko.simpleregister.service", "pl.mbatko.simpleregister.dao", "pl.mbatko.simpleregister.model"})
public class SpringRootConfig {

    @Bean
    public DataSource dataSource() {
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();

        return builder
                .setType(EmbeddedDatabaseType.HSQL)
                .setName("test")
                .addScript("db/sql/setup-db.sql")
                .build();
    }

    @Bean
    public SessionFactory sessionFactory() {
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource());

        return builder.scanPackages("pl.mbatko.simpleregister.model")
                .addProperties(getHibernateProperties())
                .buildSessionFactory();
    }

    private Properties getHibernateProperties() {
        Properties prop = new Properties();
        prop.put("hibernate.format_sql", "true");
        prop.put("hibernate.show_sql", "true");
        prop.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

        return prop;
    }

    @Bean
    public HibernateTransactionManager transactionManager() {
        return new HibernateTransactionManager(sessionFactory());
    }

//    @PostConstruct    //uncomment to run gui manager for hdsql db
//    public void startDBManager() {
//        DatabaseManagerSwing.main(new String[]{"--url", "jdbc:hsqldb:mem:test", "--user", "sa", "--password", ""});
//    }
}
