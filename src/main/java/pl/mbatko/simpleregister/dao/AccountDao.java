package pl.mbatko.simpleregister.dao;

import pl.mbatko.simpleregister.model.Account;

import java.util.List;

public interface AccountDao {

    List<Account> readAll();

    Account read(Long id);

    void save(Account account);

    boolean isNameUnique(String username);
}

