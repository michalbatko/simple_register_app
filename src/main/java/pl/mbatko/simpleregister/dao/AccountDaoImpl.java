package pl.mbatko.simpleregister.dao;

import pl.mbatko.simpleregister.model.Account;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class AccountDaoImpl implements AccountDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings(value = "unchecked")
    public List<Account> readAll() {
        return (List<Account>) sessionFactory.getCurrentSession().createCriteria(Account.class).list();
    }

    @Override
    public Account read(Long id) {
        return sessionFactory.getCurrentSession().get(Account.class, id);
    }

    @Override
    public void save(Account account) {
        sessionFactory.getCurrentSession().save(account);
    }

    @Override
    public boolean isNameUnique(String username) {
        Query query = sessionFactory.getCurrentSession().createQuery("select count(id) from Account a where a.username=:username");
        query.setString("username", username);

        return query.uniqueResult().equals(0L);
    }
}
