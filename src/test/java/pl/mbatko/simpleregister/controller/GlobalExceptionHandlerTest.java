package pl.mbatko.simpleregister.controller;

import pl.mbatko.simpleregister.config.SpringTestRootConfig;
import pl.mbatko.simpleregister.config.SpringWebConfig;
import pl.mbatko.simpleregister.service.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class, SpringTestRootConfig.class})
@WebAppConfiguration
public class GlobalExceptionHandlerTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = webAppContextSetup(wac).build();
    }

    @Test
    public void shouldDisplayErrorPageWhenUnexpectedAppError() throws Exception {
        doThrow(new IllegalStateException("unknown problem...")).when(accountService).isNameUnique(any());

        mockMvc.perform(post("/register")
                        .param("username", "johnny")
                        .param("password", "aB345678"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"));
    }
}