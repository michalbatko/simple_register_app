package pl.mbatko.simpleregister.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import pl.mbatko.simpleregister.config.SpringTestRootConfig;
import pl.mbatko.simpleregister.config.SpringWebConfig;
import pl.mbatko.simpleregister.other.Constants;
import pl.mbatko.simpleregister.service.AccountService;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class, SpringTestRootConfig.class})
@WebAppConfiguration
public class AccountControllerTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        Mockito.reset(accountService);
        mockMvc = webAppContextSetup(wac).build();
    }

    @Test
    public void shouldDisplayRegisterForm() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("account"))
                .andExpect(view().name("register"));
    }

    @Test
    public void shouldSubmitValidForm() throws Exception {
        //given
        when(accountService.isNameUnique("johnny")).thenReturn(true);

        //then
        mockMvc.perform(post("/register")
                .param("username", "johnny")
                .param("password", "aB345678"))
                .andExpect(status().isOk())
                .andExpect(view().name("success"));
    }

    @Test
    public void shouldValidateInputDataOnSubmit() throws Exception {
        mockMvc.perform(post("/register")
                .param("username", "john")
                .param("password", "pass"))
                .andExpect(status().isOk())
                .andExpect(view().name("register"))
                .andExpect(model().attributeHasFieldErrorCode("webAccount", Constants.FIELD_USERNAME, Constants.MSG_USERNAME_INVALID))
                .andExpect(model().attributeHasFieldErrorCode("webAccount", Constants.FIELD_PASSWORD, Constants.MSG_PASSWORD_INVALID));
    }


    @Test
    public void shouldValidateInputDataWithErrors() throws Exception {
        mockMvc.perform(post("/validate")
                .param("username", "john")
                .param("password", "pass"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.result", is("FAIL")))
                .andExpect(jsonPath("$.items", hasSize(2)))
                .andExpect(jsonPath("$.items[*].field", contains("username", "password")))
                .andExpect(jsonPath("$.items[*].code", contains("username.invalid", "password.invalid")));
    }

    @Test
    public void shouldValidateInputDataWithSuccess() throws Exception {
        //given
        when(accountService.isNameUnique("johnny")).thenReturn(true);

        //then
        mockMvc.perform(post("/validate")
                .param("username", "johnny")
                .param("password", "passW0rd"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.result", is("SUCCESS")))
                .andExpect(jsonPath("$.items", hasSize(0)));
    }

    @Test
    public void shouldValidateConstraintError() throws Exception {
        //given
        when(accountService.isNameUnique("johnny")).thenReturn(true);
        doThrow(new ConstraintViolationException("in a meantime our username was reserved in another session...", null))
                .when(accountService).save(any());

        //then
        mockMvc.perform(post("/register")
                .param("username", "johnny")
                .param("password", "aB345678"))
                .andExpect(status().isOk())
                .andExpect(view().name("register"))
                .andExpect(model().attributeHasFieldErrorCode("webAccount", Constants.FIELD_USERNAME, Constants.MSG_USERNAME_NOT_UNIQUE));
    }
}