package pl.mbatko.simpleregister.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.mbatko.simpleregister.config.SpringRootConfig;
import pl.mbatko.simpleregister.model.Account;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringRootConfig.class})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class AccountHibernateDaoTest {

    @Autowired
    private AccountDao accountDao;

    @Test
    public void shouldSaveAndReadAllAccounts() throws Exception {
        //given
        Account account1 = new Account("user1", "pass1");
        Account account2 = new Account("user2", "pass2");
        Account account3 = new Account("user3", "pass3");

        //when
        List<Account> accountsBefore = accountDao.readAll();

        accountDao.save(account1);
        accountDao.save(account2);
        accountDao.save(account3);
        List<Account> accountsAfter = accountDao.readAll();

        //then
        assertEquals(0, accountsBefore.size());

        assertEquals(3, accountsAfter.size());
        assertThat(accountsAfter, hasItem(account1));
        assertThat(accountsAfter, hasItem(account2));
        assertThat(accountsAfter, hasItem(account3));
    }

    @Test
    public void shouldSaveAndReadAccount() throws Exception {
        //given
        Account account = new Account("user1", "pass1");

        //when
        accountDao.save(account);
        Long id = account.getId();

        Account accountAfter = accountDao.read(id);

        //then
        assertThat(accountAfter, equalTo(account));
    }

    @Test
    public void shouldCheckNameIsUnique() throws Exception {
        //given
        Account account = new Account("user1", "pass1");

        //when
        boolean uniqueBefore = accountDao.isNameUnique(account.getUsername());

        accountDao.save(account);
        boolean uniqueAfter = accountDao.isNameUnique(account.getUsername());

        //then
        assertTrue(uniqueBefore);
        assertFalse(uniqueAfter);
    }
}