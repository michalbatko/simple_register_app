package pl.mbatko.simpleregister.validator;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class PasswordValidatorTest {

    private static final String MSG_INVALID = "password.invalid";

    private PasswordValidator validator = new PasswordValidator();

    @Test
    public void shouldPassWhenCorrectPassword() throws Exception {
        //when
        Optional<String> result1 = validator.validate("aB123456");
        Optional<String> result2 = validator.validate("abvDEF123");
        Optional<String> result3 = validator.validate("!@#$%^&*()aA1");

        //then
        assertFalse(result1.isPresent());
        assertFalse(result2.isPresent());
        assertFalse(result3.isPresent());
    }

    @Test
    public void shouldFailWhenIncorrectPassword() throws Exception {
        //given
        String[] incorrectPasswords =
                {null, "", "short", "sshortt", "onlysmallletters", "ONLYCAPITALLETTERS", "12345678",
                        "smalllettersand1234", "CAPITALAND1323", "mixEDlettERSnoNumbers", "space inside"};

        //when
        Stream.of(incorrectPasswords).forEach(pass -> {
            Optional<String> error = validator.validate(pass);

            //then
            assertThat("pass = " + pass, error.isPresent(), is(true));
            assertThat("pass = " + pass, error.get(), equalTo(MSG_INVALID));
        });
    }
}