package pl.mbatko.simpleregister.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.validation.Errors;
import pl.mbatko.simpleregister.model.WebAccount;

import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountValidatorTest {

    @Mock
    private Errors errors;

    @Mock
    private UsernameValidator usernameValidator;

    @Mock
    private PasswordValidator passwordValidator;

    @InjectMocks
    private AccountValidator accountValidator;

    @Test
    public void shouldPassForValidInput() throws Exception {
        //given
        WebAccount account = new WebAccount("username", "pAssw0rd");

        when(usernameValidator.validate(account.getUsername())).thenReturn(Optional.empty());
        when(passwordValidator.validate(account.getPassword())).thenReturn(Optional.empty());

        //when
        accountValidator.validate(account, errors);

        //then
        verifyNoMoreInteractions(errors);
    }

    @Test
    public void shouldRejectValuesWhenValidatorsFail() throws Exception {
        //given
        WebAccount account = new WebAccount("login", "pass");
        String errorMsgA = "msga";
        String errorMsgB = "msgb";

        when(usernameValidator.validate(account.getUsername())).thenReturn(Optional.of(errorMsgA));
        when(passwordValidator.validate(account.getPassword())).thenReturn(Optional.of(errorMsgB));

        //when
        accountValidator.validate(account, errors);

        //then
        verify(errors).rejectValue("username", errorMsgA);
        verify(errors).rejectValue("password", errorMsgB);
        verifyNoMoreInteractions(errors);
    }
}