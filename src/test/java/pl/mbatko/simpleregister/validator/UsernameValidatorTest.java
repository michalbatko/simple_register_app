package pl.mbatko.simpleregister.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.mbatko.simpleregister.service.AccountService;

import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsernameValidatorTest {

    private static final String MSG_INVALID = "username.invalid";
    private static final String MSG_NOT_UNIQUE = "username.notunique";

    @Mock
    private AccountService accountService;

    @InjectMocks
    private UsernameValidator validator;

    @Test
    public void shouldFailOnIncorrectUsername() throws Exception {
        //given
        when(accountService.isNameUnique(any())).thenReturn(true);

        String[] incorrectLogins =
                {null, "", "short", "inv@lid", "INV.ALID", "INVAL ID"};

        //when
        Stream.of(incorrectLogins).forEach(login -> {
            Optional<String> error = validator.validate(login);

            //then
            assertThat("login = " + login, error.isPresent(), is(true));
            assertThat("login = " + login, error.get(), equalTo(MSG_INVALID));
        });
    }

    @Test
    public void shouldFailOnNonUniqueUsername() throws Exception {
        //given
        when(accountService.isNameUnique("duplicate")).thenReturn(false);

        //when
        Optional<String> error = validator.validate("duplicate");

        //then
        assertTrue(error.isPresent());
        assertEquals(MSG_NOT_UNIQUE, error.get());
    }

    @Test
    public void shouldPassWhenCorrectUniqueUsername() throws Exception {
        //given
        when(accountService.isNameUnique(any())).thenReturn(true);

        //when
        Optional<String> error1 = validator.validate("johnny");
        Optional<String> error2 = validator.validate("admin3");

        //then
        assertFalse(error1.isPresent());
        assertFalse(error2.isPresent());
    }
}