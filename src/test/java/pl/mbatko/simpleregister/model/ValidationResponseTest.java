package pl.mbatko.simpleregister.model;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class ValidationResponseTest {

    @Test
    public void shouldContainSuccessResult() throws Exception {
        //given
        ValidationResponse response = ValidationResponse.success();

        //then
        assertThat(response.getResult(), equalTo("SUCCESS"));
        assertThat(response.getItems(), hasSize(0));
    }

    @Test
    public void shouldContainErrorItem() throws Exception {
        //given
        ValidationResponse response = ValidationResponse.fail();

        //when
        response.addItem("field", "code", "message");

        //then
        assertThat(response.getResult(), equalTo("FAIL"));
        assertThat(response.getItems(), hasSize(1));

        ValidationItem item = response.getItems().get(0);
        assertThat(item.getField(), equalTo("field"));
        assertThat(item.getCode(), equalTo("code"));
        assertThat(item.getMessage(), equalTo("message"));
    }
}