package pl.mbatko.simpleregister.config;

import pl.mbatko.simpleregister.service.AccountService;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringTestRootConfig {

    public SpringTestRootConfig() {
        MockitoAnnotations.initMocks(this);
    }

    @Bean
    public AccountService getAccountService() {
        return Mockito.mock(AccountService.class);
    }
}
