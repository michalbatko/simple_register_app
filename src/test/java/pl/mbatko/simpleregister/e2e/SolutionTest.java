package pl.mbatko.simpleregister.e2e;

import pl.mbatko.simpleregister.config.SpringRootConfig;
import pl.mbatko.simpleregister.config.SpringWebConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static pl.mbatko.simpleregister.other.Constants.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringRootConfig.class, SpringWebConfig.class})
@WebAppConfiguration
public class SolutionTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(wac).build();
    }

    @Test
    public void shouldDisplayRegisterForm() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(status().isOk())
                .andExpect(view().name("register"));
    }

    @Test
    public void shouldSubmitRegisterForm() throws Exception {
        mockMvc.perform(post("/register")
                    .param("username", "johnny")
                    .param("password", "aB345678"))
                .andExpect(status().isOk())
                .andExpect(view().name("success"));
    }

    @Test
    public void shouldFireValidation() throws Exception {
        mockMvc.perform(post("/register")
                    .param("username", "test")
                    .param("password", "test"))
                .andExpect(status().isOk())
                .andExpect(model().attributeHasFieldErrorCode("webAccount", FIELD_USERNAME, MSG_USERNAME_INVALID))
                .andExpect(model().attributeHasFieldErrorCode("webAccount", FIELD_PASSWORD, MSG_PASSWORD_INVALID))
                .andExpect(view().name("register"));
    }

}