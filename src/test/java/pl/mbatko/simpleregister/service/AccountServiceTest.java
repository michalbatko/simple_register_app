package pl.mbatko.simpleregister.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.mbatko.simpleregister.dao.AccountDao;
import pl.mbatko.simpleregister.model.Account;
import pl.mbatko.simpleregister.model.WebAccount;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountDao accountDao;

    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    @Test
    public void shouldSaveAccount() throws Exception {
        //given
        WebAccount webAccount = new WebAccount("user1", "pass1");
        Account account = new Account("user1", "pass1");

        //when
        accountService.save(webAccount);

        //then
        verify(accountDao).save(account);
    }

    @Test
    public void shouldCheckNameIsUnique() throws Exception {
        //given
        String username1 = "user1";
        String username2 = "user2";
        when(accountDao.isNameUnique(username1)).thenReturn(true);
        when(accountDao.isNameUnique(username2)).thenReturn(false);

        //when
        boolean uniqueName = accountService.isNameUnique(username1);
        boolean uniqueName2 = accountService.isNameUnique(username2);

        //then
        assertTrue(uniqueName);
        assertFalse(uniqueName2);
        verify(accountDao).isNameUnique(username1);
        verify(accountDao).isNameUnique(username2);
    }
}