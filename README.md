# Simple Register #

Simple web application for basic account registration (HTML, CSS, jQuery, Bootstrap, Spring MVC, Hibernate, HSQLDB).

 
### Setup ###

After checkout, build with Maven:
```
mvn clean install
```
and deploy **simple-register-app-1.0-SNAPSHOT.war** to Tomcat.

In web browser type address similar to:
```
http://localhost:8080/simple-register-app-1.0-SNAPSHOT/register
```
# #
![simple.png](https://bitbucket.org/repo/56qygG/images/2860956913-simple.png)